//
//  AppDelegate.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/4/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        return true
    }
}

