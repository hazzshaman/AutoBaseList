//
//  AutoSQLiteBase.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/7/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation
import SQLite

class SQLiteCarsGateway: CarsGateway {

    func fetchCars(completionHandler: @escaping FetchCarsEntityGatewayCompletionHandler){
        SQLiteDataBase.sharedInstance.getCars(completion: completionHandler)
    }

    func add(parameters: AddCarParameters, completionHandler: @escaping AddCarEntityGatewayCompletionHandler){
        SQLiteDataBase.sharedInstance.addCar(parameters: parameters, completion: completionHandler)
    }

    func delete(car: Car, completionHandler: @escaping DeleteCarEntityGatewayCompletionHandler){
        SQLiteDataBase.sharedInstance.deleteCar(cid: Int64(car.id), complition: completionHandler)
    }

    func update(car: Car, parameters: AddCarParameters,
                completionHandler: @escaping UpdateCarEntityGatewayCompletionHandler){
        SQLiteDataBase.sharedInstance.updateCar(cid: Int64(car.id),
                                                parameters: parameters,
                                                complition: completionHandler)
    }
}

class SQLiteDataBase {

    // MARK: Properties

    static let sharedInstance = SQLiteDataBase()
    let db: Connection?

    private let cars = Table("Cars")
    private let id = Expression<Int64>("id")
    private let title = Expression<String>("title")
    private let color = Expression<String>("color")
    private let idOwner = Expression<Int64>("idOwner")

    private let owners = Table("Owners")
    private let idOwners = Expression<Int64>("idOwner")
    private let fName = Expression<String>("fName")
    private let lName = Expression<String>("lName")
    private let age = Expression<Int64>("age")

    // MARK: Life cycle

    private init() {

        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        do {
            db = try Connection("\(path)/AutoDB.sqlite3")
        } catch {
            db = nil
        }
        createTableCars()
        createTableOwner()
    }

    func createTableCars() {
        do {
            try db!.run(cars.create(ifNotExists: true) { table in
                table.column(id, primaryKey: true)
                table.column(title)
                table.column(color)
                table.column(idOwner)
            })
        } catch {
            print("Unable to create table")
        }
    }

    func createTableOwner() {
        do {
            try db!.run(owners.create(ifNotExists: true) { table in
                table.column(idOwners, primaryKey: true)
                table.column(fName)
                table.column(lName)
                table.column(age)
            })
        } catch {
            print("Unable to create table")
        }
    }

    // MARK: Gateway Actions

    func addCar(parameters: AddCarParameters, completion: @escaping (_ car: Result<Car>) -> Void) {
        do {

            let insertOwner = owners.insert(fName <- parameters.owner.fname,
                                            lName <- parameters.owner.lname,
                                            age <- Int64(parameters.owner.age))
            let newIdOwner = try db!.run(insertOwner)

            let insertCar = cars.insert(title <- parameters.title,
                                        color <- parameters.color,
                                        idOwner <- newIdOwner)

            let newIdCar = try db!.run(insertCar)

            let car = Car(id: Int(newIdCar),
                          title: parameters.title,
                          color: parameters.color,
                          owner: parameters.owner)
            completion(.success(car))
        } catch {
            completion(.failure(CoreError()))
        }
    }

    func getCars(completion: @escaping (_ cars: Result<[Car]>) -> Void) {
        var cars: [Car] = []
        var owner = Owner(lname: "", fname: "", age: 0)

        do {
            for car in try db!.prepare(self.cars) {
                for carOwner in try db!.prepare(self.owners.filter(idOwners == car[id])) {
                    owner = Owner(lname: carOwner[lName],
                               fname: carOwner[fName],
                               age: Int(carOwner[age]))
                }
                cars.append(Car(id: Int(car[id]), title: car[title], color: car[color], owner: owner))
            }
        } catch {
            completion(.failure(CoreError()))
        }
        completion(.success(cars))
    }

    func deleteCar(cid: Int64, complition: @escaping (_ car: Result<Void>) -> Void) {
        do {
            let car = cars.filter(id == cid)
            var idCarOwner: Int64 = cid

            for item in try db!.prepare(car) {
                idCarOwner = Int64(item[id])
            }
            
            let owner = owners.filter(idOwners == idCarOwner)

            try db!.run(owner.delete())
            try db!.run(car.delete())

            complition(.success())
        } catch {
            complition(.failure(CoreError()))
        }
    }

    func updateCar(cid: Int64, parameters: AddCarParameters, complition: @escaping (_ car: Result<Void>) -> Void) {
        do {
            let car = cars.filter(id == cid)
            var idCarOwner: Int64 = cid

            for item in try db!.prepare(car) {
                idCarOwner = Int64(item[id])
            }

            let owner = owners.filter(idOwners == idCarOwner)

            let updateCar = car.update([
                title <- parameters.title,
                color <- parameters.color,
                idOwner <- car[idOwner]
                ])
            
            let updateOwner = owner.update([
                fName <- parameters.owner.fname,
                lName <- parameters.owner.lname,
                age <- Int64(parameters.owner.age)
                ])

            if try db!.run(updateCar) > 0 && db!.run(updateOwner) > 0 {
                complition(.success())
            }
        } catch {
            complition(.failure(CoreError()))
        }
    }
}
