//
//  CarDetailsPresenter.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/7/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol CarDetailsView: class {
    func display(id: String)
    func display(title: String)
    func display(color: String)
    func display(ownerFName: String)
    func display(ownerLName: String)
    func display(ownerAge: String)
    func displayUpdateCarError(title: String, message: String)
    func updateCancelButtonState(isEnabled enabled: Bool)
}

protocol CarDetailsPresenter {
    var router: CarDetailsViewRouter { get }
    func viewDidLoad()
    func updateButtonPressed(parameters: AddCarParameters)
}

class CarDetailsPresenterImplementation {

    // MARK: Properties

    let car: Car
    let updateCarUseCase: UpdateCarUseCase
    let router: CarDetailsViewRouter
    weak var view: CarDetailsView?

    // MARK: Life Cycle

    init(view: CarDetailsView,
         car: Car,
         updateCarUseCase: UpdateCarUseCase,
         router: CarDetailsViewRouter) {
        self.car = car
        self.view = view
        self.updateCarUseCase = updateCarUseCase
        self.router = router
    }

    func viewDidLoad() {
        view?.display(id: String(car.id))
        view?.display(title: car.title)
        view?.display(color: car.color)
        view?.display(ownerFName: car.owner.fname)
        view?.display(ownerLName: car.owner.lname)
        view?.display(ownerAge: String(car.owner.age))
    }

    func handleCarUpdate() {
        router.dismissView()
    }

    func handleCarUpdateError(_ error: Error ) {
        view?.displayUpdateCarError(title: "Error", message: error.localizedDescription)
    }
}

// MARK: CarDetailsPresenter

extension CarDetailsPresenterImplementation: CarDetailsPresenter{

    func updateButtonPressed(parameters: AddCarParameters) {

        updateCarUseCase.update(car: car, parameters: parameters) { (result) in
            switch result {
            case .success(_):
                self.handleCarUpdate()
            case .failure(let error):
                self.handleCarUpdateError(error)
            }
        }
    }
}
