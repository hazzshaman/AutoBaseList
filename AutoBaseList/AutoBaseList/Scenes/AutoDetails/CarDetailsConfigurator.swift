//
//  CarDetailsConfigurator.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/7/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol CarDetailsConfigurator {
    func configure(carDetailsTableViewController: CarDetailsTableViewController)
}

class CarDetailsConfiguratorImplementation: CarDetailsConfigurator {

    let car: Car

    init(car: Car) {
        self.car = car
    }

    func configure(carDetailsTableViewController: CarDetailsTableViewController) {

        let sqlite = SQLiteCarsGateway()
        let updateCarUseCase = UpdateCarUseCaseImplementation(carsGateway: sqlite)
        let router = CarDetailsViewRouterImplementation(carDetailsTableViewController: carDetailsTableViewController)
        let presenter = CarDetailsPresenterImplementation(view: carDetailsTableViewController,
                                                          car: car,
                                                          updateCarUseCase: updateCarUseCase,
                                                          router: router)

        carDetailsTableViewController.presenter = presenter
    }
}
