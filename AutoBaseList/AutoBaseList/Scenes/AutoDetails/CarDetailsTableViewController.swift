//
//  CarDetailsTableViewController.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import UIKit

class CarDetailsTableViewController: UITableViewController {
    var presenter: CarDetailsPresenter!
    var configurator: CarDetailsConfigurator!

    // MARK: - IBOutlets

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var ownerFNameTextField: UITextField!
    @IBOutlet weak var ownerLNameTextField: UITextField!
    @IBOutlet weak var ownerAgeTextField: UITextField!
    @IBOutlet weak var updateButton: UIBarButtonItem!

    // MARK: - IBAction

    @IBAction func updateButtonPressed(_ sender: Any) {

        let owner = Owner(lname: ownerLNameTextField.text ?? "",
                          fname: ownerFNameTextField.text ?? "" ,
                          age: Int(ownerAgeTextField.text ?? "") ?? 0)

        let addCarParameters = AddCarParameters(title: titleTextField.text ?? "",
                                                color: colorTextField.text ?? "",
                                                owner: owner)

        presenter.updateButtonPressed(parameters: addCarParameters)
    }

    // MARK: Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configurator.configure(carDetailsTableViewController: self)
        presenter.viewDidLoad()
    }
}

 // MARK: - CarDetailsView

extension CarDetailsTableViewController: CarDetailsView {

    func display(id: String) {
        idLabel.text = id
    }

    func display(title: String) {
        titleTextField.text = title
    }

    func display(color: String) {
        colorTextField.text = color
    }

    func display(ownerFName: String) {
        ownerFNameTextField.text = ownerFName
    }

    func display(ownerLName: String) {
        ownerLNameTextField.text = ownerLName
    }

    func display(ownerAge: String) {
        ownerAgeTextField.text = ownerAge
    }

    func displayUpdateCarError(title: String, message: String) {
        presentAlert(withTitle: title, message: message)
    }

    func updateCancelButtonState(isEnabled enabled: Bool) {
        updateButton.isEnabled = enabled
    }
}
