//
//  CarDetailsViewRouter.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol CarDetailsViewRouter: ViewRouter {
    func dismissView()
}

class CarDetailsViewRouterImplementation: CarDetailsViewRouter {
    weak var carDetailsTableViewController: CarDetailsTableViewController?

    init(carDetailsTableViewController: CarDetailsTableViewController) {
        self.carDetailsTableViewController = carDetailsTableViewController
    }

    func dismissView() {
        let _ = carDetailsTableViewController?.navigationController?.popViewController(animated: true)
    }
}
