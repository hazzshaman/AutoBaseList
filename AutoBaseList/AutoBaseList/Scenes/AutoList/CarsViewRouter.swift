//
//  CarsViewRouter.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import UIKit

protocol CarsViewRouter: ViewRouter {
    func presentDetailsView(for car: Car)
    func presentAddCar(addCarPresenterDelegate: AddCarPresenterDelegate)
}

class CarsViewRouterImplementaion: CarsViewRouter {
    weak var carsTableViewController: CarsTableViewController?
    weak var addCarPresenterDelegate: AddCarPresenterDelegate?
    var car: Car!

    init(carsTableViewController: CarsTableViewController) {
        self.carsTableViewController = carsTableViewController
    }

    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let carDetailsTableViewController = segue.destination
            as? CarDetailsTableViewController {
            carDetailsTableViewController.configurator = CarDetailsConfiguratorImplementation(car: car)
        } else if let navigationController = segue.destination as? UINavigationController,
            let addCarViewController = navigationController.topViewController as? AddCarViewController { addCarViewController.configurator = AddCarConfiguratorImplementation(addCarPresenterDelegate: addCarPresenterDelegate)
        }
    }

    // MARK: - CarsViewRouter

    func presentDetailsView(for car: Car) {
        self.car = car
        carsTableViewController?.performSegue(withIdentifier: "CarsSceneToCarDetailsSceneSegue", sender: nil)
    }

    func presentAddCar(addCarPresenterDelegate: AddCarPresenterDelegate) {
        self.addCarPresenterDelegate = addCarPresenterDelegate
        carsTableViewController?.performSegue(withIdentifier: "CarsSceneToAddCarSceneSegue", sender: nil)
    }
}


