//
//  CarsConfigurator.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol CarsConfigurator {
    func configure(carsTableViewController: CarsTableViewController)
}

class CarsConfiguratorImplementation: CarsConfigurator {
    func configure(carsTableViewController: CarsTableViewController) {

        let sqliteGateway = SQLiteCarsGateway()
        let displayCarsUseCase = DisplayCarsUseCaseImplementaion(carsGateway: sqliteGateway)
        let deleteCarUseCase = DeleteCarUseCaseImplementation(carsGateway: sqliteGateway)
        let router = CarsViewRouterImplementaion(carsTableViewController: carsTableViewController)
        let presenter = CarsPresenterImplemetation(view: carsTableViewController,
                                                   displayCarsUseCase: displayCarsUseCase,
                                                   deleteCarUseCase: deleteCarUseCase,
                                                   router: router)

        carsTableViewController.presenter = presenter
    }
}
