//
//  CarsTableViewController.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import UIKit

class CarsTableViewController: UITableViewController {

    // MARK: Properties

    var configurator = CarsConfiguratorImplementation()
    var presenter: CarsPresenter!

    // MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configurator.configure(carsTableViewController: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        presenter.viewDidLoad()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        presenter.router.prepare(for: segue, sender: sender)
    }

    // MARK: - IBAction

    @IBAction func addButtonPressed(_ sender: Any) {
        presenter.addButtonPressed()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return presenter.numberOfCars
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CarTableViewCell",
                                                       for: indexPath) as? CarTableViewCell else {
            fatalError("Problem with unwraping cell to CarTableViewCell")
        }

        presenter.configure(cell: cell, forRow: indexPath.row)

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelect(row: indexPath.row)
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return presenter.canEdit(row: indexPath.row)
    }

    override func tableView(_ tableView: UITableView,
                            titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return presenter.titleForDeleteButton(row: indexPath.row)
    }

    override func tableView(_ tableView: UITableView,
                            commit editingStyle: UITableViewCellEditingStyle,
                            forRowAt indexPath: IndexPath) {
        presenter.deleteButtonPressed(row: indexPath.row)
    }
}

// MARK: - CarsView

extension CarsTableViewController: CarsView {

    func refreshCarsView() {
        tableView.reloadData()
    }

    func displayCarsRetrievalError(title: String, message: String) {
        presentAlert(withTitle: title, message: message)
    }

    func displayCarDeleteError(title: String, message: String) {
        presentAlert(withTitle: title, message: message)
    }

    func deleteAnimated(row: Int) {
        tableView.deleteRows(at: [IndexPath(row: row, section: 0)], with: .automatic)
    }

    func endEditing() {
        tableView.setEditing(false, animated: true)
    }
}
