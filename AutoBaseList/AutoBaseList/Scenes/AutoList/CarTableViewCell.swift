//
//  CarTableViewCell.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell, CarCellView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!


    func display(id: String) {
        idLabel.text = id
    }

    func display(title: String) {
        titleLabel.text = title
    }

}
