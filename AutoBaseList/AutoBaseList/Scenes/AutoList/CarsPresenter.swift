//
//  CarsPresenter.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol CarsView: class {
    func refreshCarsView()
    func displayCarsRetrievalError(title: String, message: String)
    func displayCarDeleteError(title: String, message: String)
    func deleteAnimated(row: Int)
    func endEditing()
}

protocol CarCellView {
    func display(title: String)
    func display(id: String)
}

protocol CarsPresenter {
    var numberOfCars: Int { get }
    var router: CarsViewRouter { get }
    func viewDidLoad()
    func configure(cell: CarCellView, forRow row: Int)
    func didSelect(row: Int)
    func canEdit(row: Int) -> Bool
    func titleForDeleteButton(row: Int) -> String
    func deleteButtonPressed(row: Int)
    func addButtonPressed()
}

class CarsPresenterImplemetation: CarsPresenter {

    // MARK: Properties

    weak var view: CarsView?
    let displayCarsUseCase: DisplayCarsUseCase
    let deleteCarUseCase: DeleteCarUseCase
    let router: CarsViewRouter

    var cars:[Car] = []

    var numberOfCars: Int {
        return cars.count
    }

    init(view: CarsView,
         displayCarsUseCase: DisplayCarsUseCase,
         deleteCarUseCase: DeleteCarUseCase,
         router: CarsViewRouter) {
        self.view = view
        self.displayCarsUseCase = displayCarsUseCase
        self.deleteCarUseCase = deleteCarUseCase
        self.router = router
    }

    // MARK: - CarsPresenter

    func viewDidLoad() {
        self.displayCarsUseCase.displayCars { (result) in
            switch result {
            case .success(let cars):
                self.handleCarsReceived(cars)
            case .failure(let error):
                self.handleCarsError(error)
            }
        }
    }

    func configure(cell: CarCellView, forRow row: Int) {
        let car = cars[row]

        cell.display(title: car.title)
        cell.display(id: String(car.id))
    }

    func didSelect(row: Int) {
        let car = cars[row]

        router.presentDetailsView(for: car)
    }

    func canEdit(row: Int) -> Bool {
        return true
    }

    func titleForDeleteButton(row: Int) -> String {
        return "Delete"
    }

    func deleteButtonPressed(row: Int) {
        view?.endEditing()

        let car = cars[row]
        self.deleteCarUseCase.delete(car: car) { (result) in

            switch result {
            case .success():
                self.handleCarDeleted(car: car)
            case .failure(let error):
                self.handleCarsError(error)
            }
        }
    }

    func addButtonPressed() {
        router.presentAddCar(addCarPresenterDelegate: self)
    }

    func handleCarsReceived(_ cars: [Car]) {
        self.cars = cars
        view?.refreshCarsView()
    }

    func handleCarsError(_ error: Error) {
        view?.displayCarsRetrievalError(title: "Error", message: error.localizedDescription)
    }

    func handleCarDeleted(car: Car) {

        if let row = cars.index(of: car) {
            cars.remove(at: row)
            view?.deleteAnimated(row: row)
        }
    }
}

// MARK: - AddCarPresenterDelegate

extension CarsPresenterImplemetation: AddCarPresenterDelegate {

    func addCarPresenter(_ presenter: AddCarPresenter, didAdd car: Car) {
        presenter.router.dismiss()
        cars.append(car)
        view?.refreshCarsView()
    }

    func addCarPresenterCancel(presenter: AddCarPresenter) {
        presenter.router.dismiss()
    }
}
