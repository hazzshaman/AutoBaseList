//
//  AddCarConfigurator.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol AddCarConfigurator {
    func configure(addCarViewController: AddCarViewController)
}

class AddCarConfiguratorImplementation: AddCarConfigurator {
    var addCarPresenterDelegate: AddCarPresenterDelegate?

    init(addCarPresenterDelegate: AddCarPresenterDelegate?) {
        self.addCarPresenterDelegate = addCarPresenterDelegate
    }

    func configure(addCarViewController: AddCarViewController) {

        let sqlite = SQLiteCarsGateway()
        let addCarUseCase = AddCarUseCaseImplementation(carsGateway: sqlite)
        let router = AddCarViewRouterImplemetation(addCarViewController: addCarViewController)
        let presenter = AddCarPresenterImplementation(view: addCarViewController, addCarUseCase: addCarUseCase, router: router, delegate: addCarPresenterDelegate)

        addCarViewController.presenter = presenter
    }
}
