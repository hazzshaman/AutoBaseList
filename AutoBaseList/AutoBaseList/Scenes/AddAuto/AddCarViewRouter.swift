//
//  AddCarViewRouter.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol AddCarViewRouter: ViewRouter {
    func dismiss()
}

class AddCarViewRouterImplemetation: AddCarViewRouter {
    weak var addCarViewController: AddCarViewController?

    init(addCarViewController: AddCarViewController) {
        self.addCarViewController = addCarViewController
    }

    // MARK: AddCarRouter

    func dismiss() {
        addCarViewController?.dismiss(animated: true, completion: nil)
    }
}
