//
//  AddCarViewController.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import UIKit

class AddCarViewController: UIViewController {
    var presenter: AddCarPresenter!
    var configurator: AddCarConfigurator!

    // MARK: - IBOutlets

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var colorTextField: UITextField!
    @IBOutlet weak var ownerFNameTextField: UITextField!
    @IBOutlet weak var ownerLNameTextField: UITextField!
    @IBOutlet weak var ownerAgeTextField: UITextField!

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        configurator.configure(addCarViewController: self)
    }

    // MARK: - IBActions

    @IBAction func saveButtonPressed(_ sender: Any) {

        let owner = Owner(lname: ownerLNameTextField.text ?? "",
                          fname: ownerFNameTextField.text ?? "" ,
                          age: Int(ownerAgeTextField.text ?? "") ?? 0)

        let addCarParameters = AddCarParameters(title: titleTextField.text ?? "",
                                                color: colorTextField.text ?? "",
                                                owner: owner)
        
        presenter.addButtonPressed(parameters: addCarParameters)
    }

    @IBAction func cancelButtonPressed(_ sender: Any) {
        presenter.cancelButtonPressed()
    }
}

      // MARK: - AddCarView

extension AddCarViewController: AddCarView {

    func updateAddButtonState(isEnabled enabled: Bool) {
        cancelButton.isEnabled = enabled
    }

    func updateCancelButtonState(isEnabled enabled: Bool) {
        saveButton.isEnabled = enabled
    }

    func displayAddCarError(title: String, message: String) {
        presentAlert(withTitle: title, message: message)
    }
}
