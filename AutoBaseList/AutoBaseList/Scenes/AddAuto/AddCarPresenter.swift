//
//  AddCarPresenter.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/6/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

protocol AddCarView: class {
    func updateAddButtonState(isEnabled enabled: Bool)
    func updateCancelButtonState(isEnabled enabled: Bool)
    func displayAddCarError(title: String, message: String)
}

protocol AddCarPresenterDelegate: class {
    func addCarPresenter(_ presenter: AddCarPresenter, didAdd car: Car)
    func addCarPresenterCancel(presenter: AddCarPresenter)
}

protocol AddCarPresenter {
    var router: AddCarViewRouter { get }
    func addButtonPressed(parameters: AddCarParameters)
    func cancelButtonPressed()
}

class AddCarPresenterImplementation: AddCarPresenter {

    // MARK: Properties

    weak var view: AddCarView?
    var addCarUseCase: AddCarUseCase
    weak var delegate: AddCarPresenterDelegate?
    var router: AddCarViewRouter

    // MARK: Life Cycle

    init(view: AddCarView,
         addCarUseCase: AddCarUseCase,
         router: AddCarViewRouter,
         delegate: AddCarPresenterDelegate?) {
        self.view = view
        self.addCarUseCase = addCarUseCase
        self.router = router
        self.delegate = delegate
    }

    fileprivate func handleCarAdded(_ car: Car) {
        delegate?.addCarPresenter(self, didAdd: car)
    }

    fileprivate func handleCarError(_ error: Error) {
        view?.displayAddCarError(title: "Error", message: error.localizedDescription)
    }

    // MARK: AddCarPresenter

    func addButtonPressed(parameters: AddCarParameters) {
        addCarUseCase.add(parameters: parameters) { (result) in
            switch result {
            case .success(let car):
                self.handleCarAdded(car)
            case .failure(let error):
                self.handleCarError(error)
            }
        }
    }

    func cancelButtonPressed() {
        delegate?.addCarPresenterCancel(presenter: self)
    }
}
