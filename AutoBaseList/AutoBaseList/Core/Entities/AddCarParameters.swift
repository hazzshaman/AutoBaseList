//
//  AddCarParameters.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

struct AddCarParameters {
    var title: String
    var color: String
    var owner: Owner
}
