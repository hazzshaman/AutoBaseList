//
//  Owner.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

struct Owner {
    var lname: String
    var fname: String
    var age: Int
}
