//
//  Car.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

struct Car {
    var id: Int
    var title: String
    var color: String
    var owner: Owner
}

extension Car: Equatable { }

func == (lhs: Car, rhs: Car) -> Bool {
    return lhs.id == rhs.id
}
