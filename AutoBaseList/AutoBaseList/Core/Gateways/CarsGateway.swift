//
//  CarsGateway.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

typealias FetchCarsEntityGatewayCompletionHandler = (_ cars: Result<[Car]>) -> Void
typealias AddCarEntityGatewayCompletionHandler = (_ car: Result<Car>) -> Void
typealias DeleteCarEntityGatewayCompletionHandler = (_ car: Result<Void>) -> Void
typealias UpdateCarEntityGatewayCompletionHandler = (_ car: Result<Void>) -> Void

protocol CarsGateway {
    func fetchCars(completionHandler: @escaping FetchCarsEntityGatewayCompletionHandler)
    func add(parameters: AddCarParameters, completionHandler: @escaping AddCarEntityGatewayCompletionHandler)
    func delete(car: Car, completionHandler: @escaping DeleteCarEntityGatewayCompletionHandler)
    func update(car: Car,
                parameters: AddCarParameters,
                completionHandler: @escaping UpdateCarEntityGatewayCompletionHandler)
}

