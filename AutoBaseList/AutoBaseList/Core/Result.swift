//
//  Result.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

struct CoreError: Error {
    var localizedDescription: String {
        return message
    }

    var message = ""
}

enum Result<T> {
    case success(T)
    case failure(Error)

    public func dematerialize() throws -> T {
        switch  self  {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }
}
