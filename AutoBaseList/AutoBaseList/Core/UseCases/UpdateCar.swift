//
//  UpdateCar.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/10/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

typealias UpdateCarUseCaseCompletionHandler = (_ cars: Result<Void>) -> Void

protocol UpdateCarUseCase {
    func update(car: Car, parameters: AddCarParameters, completionHandler: @escaping UpdateCarUseCaseCompletionHandler)
}

class UpdateCarUseCaseImplementation: UpdateCarUseCase {

    let carsGateway: CarsGateway

    init(carsGateway: CarsGateway) {
        self.carsGateway = carsGateway
    }

    // MARK: - UpdateCarUseCase

    func update(car: Car, parameters: AddCarParameters, completionHandler: @escaping (_ cars: Result<Void>) -> Void) {
        self.carsGateway.update(car: car, parameters: parameters, completionHandler: { (result) in
            completionHandler(result)
        })
    }
}
