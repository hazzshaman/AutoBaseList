//
//  DisplayCarsList.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

typealias DisplayCarsUseCaseCopletionHandler = (_ cars: Result<[Car]>) -> Void

protocol DisplayCarsUseCase {
    func displayCars(completionHandler: @escaping DisplayCarsUseCaseCopletionHandler)
}

class DisplayCarsUseCaseImplementaion: DisplayCarsUseCase {

    let carsGateway: CarsGateway

    init(carsGateway: CarsGateway) {
        self.carsGateway = carsGateway
    }

    // MARK: - DisplayCarsUseCase

    func displayCars(completionHandler: @escaping (Result<[Car]>) -> Void) {
        self.carsGateway.fetchCars { (result) in

            completionHandler(result)
        }
    }
}
