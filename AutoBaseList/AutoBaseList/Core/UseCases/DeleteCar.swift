//
//  DeleteCar.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

typealias DeleteCarUseCaseCompletionHandler = (_ cars: Result<Void>) -> Void

protocol DeleteCarUseCase {
    func delete(car: Car, completionHandler: @escaping DeleteCarUseCaseCompletionHandler)
}

class DeleteCarUseCaseImplementation: DeleteCarUseCase {

    let carsGateway: CarsGateway

    init(carsGateway: CarsGateway) {
        self.carsGateway = carsGateway
    }

    // MARK: - DeleteCarUseCase

    func delete(car: Car, completionHandler: @escaping (Result<Void>) -> Void) {

        self.carsGateway.delete(car: car) { (result) in
            
            completionHandler(result)
        }
    }
}
