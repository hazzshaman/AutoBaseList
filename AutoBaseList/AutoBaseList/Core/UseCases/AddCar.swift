//
//  AddCar.swift
//  AutoBaseList
//
//  Created by Yura Ostrovskii on 9/5/17.
//  Copyright © 2017 yuraostrovskiy85@gmail.com. All rights reserved.
//

import Foundation

typealias AddCarUseCaseCompletionHandler = (_ car: Result<Car>) -> Void

protocol AddCarUseCase {
    func add(parameters: AddCarParameters, completionHandler: @escaping AddCarUseCaseCompletionHandler)
}

class AddCarUseCaseImplementation: AddCarUseCase {

    let carsGateway: CarsGateway

    init(carsGateway: CarsGateway) {
        self.carsGateway = carsGateway
    }

    // MARK: - AddCarUseCase

    func add(parameters: AddCarParameters, completionHandler: @escaping (Result<Car>) -> Void) {
        self.carsGateway.add(parameters: parameters) { (result) in

            completionHandler(result)
        }
    }
}
